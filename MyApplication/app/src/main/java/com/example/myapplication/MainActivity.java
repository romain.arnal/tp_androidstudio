package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = (Button)findViewById(R.id.authenticate);
        b1.setOnClickListener(new View.OnClickListener() {
            private String readStream(InputStream is) throws IOException {
                StringBuilder sb = new StringBuilder();
                BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
                for (String line = r.readLine(); line != null; line =r.readLine()){
                    sb.append(line);
                }
                is.close();
                return sb.toString();
            }

            @Override
            public void onClick(View view) {
                final URL[] url = {null};
                Thread thread = new Thread() {

                    public void run() {
                        try {
                            TextView Tlogin = findViewById(R.id.login);
                            String login = Tlogin.getText().toString();
                            TextView Tpassword = findViewById(R.id.password);
                            String password = Tpassword.getText().toString();
                            String loginPassword = login + ":" + password;

                            url[0] = new URL("https://httpbin.org/basic-auth/"+login+"/"+password);
                            HttpURLConnection urlConnection = (HttpURLConnection) url[0].openConnection();
                            String basicAuth = "Basic " + Base64.encodeToString(loginPassword.getBytes(),
                                    Base64.NO_WRAP);
                            urlConnection.setRequestProperty ("Authorization", basicAuth);
                            try {
                                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                                String s = readStream(in);
                                Log.i("JFL", s);
                                JSONObject json = new JSONObject(s);
                                String res = json.getString("authenticated");
                                MainActivity.this.result = res;
                                runResultTread();
                            } catch (JSONException e) {
                                Log.i("Erreur","JSON non créé");
                                throw new RuntimeException(e);
                            } finally {
                                urlConnection.disconnect();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
            }
        });
    }

    private void runResultTread(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView result = findViewById(R.id.result);
                result.setVisibility(View.VISIBLE);
                result.setText(MainActivity.this.result);
            }
        });
    }
}