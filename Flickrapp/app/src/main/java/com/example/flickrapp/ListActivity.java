package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ListView;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        MyAdapter myAdapter = new MyAdapter(this);
        ListView itemsListView  = (ListView) findViewById(R.id.list);
        itemsListView.setAdapter(myAdapter);
        AsyncFlickrJSONDataForList asyncList = new AsyncFlickrJSONDataForList(myAdapter);
        asyncList.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
    }
}

