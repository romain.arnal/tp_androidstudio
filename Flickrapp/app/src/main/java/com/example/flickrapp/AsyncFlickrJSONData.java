package com.example.flickrapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONData extends AsyncTask<String,Void,JSONObject> {
    public ImageView i;
    public AsyncFlickrJSONData(ImageView i) {
        this.i = i;
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }


    @Override
    protected JSONObject doInBackground(String... strings) {
        Log.i("dib","start");
        JSONObject json = new JSONObject();
        final URL[] url = {null};
        try {
            String st = strings[0];
            Log.i("valeur de st",st);
            url[0] = new URL(st+"&nojsoncallback=1");
            HttpURLConnection urlConnection = (HttpURLConnection) url[0].openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String s = readStream(in);
                Log.i("yes", s.substring(1,20));
                json = new JSONObject(s);

            } catch (JSONException e) {
                Log.i("Erreur", "JSON non créé");
                throw new RuntimeException(e);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    protected void onPostExecute(JSONObject json) {
        try {
            String link = json.getJSONArray("items").getJSONObject(0).getJSONObject("media").getString("m");
            Log.i("ope","jhdvzehjvd");
            new AsyncBitmapDownloader(i).execute(link);
        } catch (JSONException e){
            Log.v("ope","erreur recupération lien image");
            throw new RuntimeException(e);
        }
    }
}

