package com.example.flickrapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncBitmapDownloader extends AsyncTask<String,Void,Bitmap> {
    private ImageView i;
    public AsyncBitmapDownloader(ImageView i) {
        this.i = i;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        final URL[] url = {null};
        Log.i("run1","start");
        try {
            url[0] = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url[0].openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                Log.i("image","fin de l'image");
                return BitmapFactory.decodeStream(in);

            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Bitmap bitmap) {
        i.setImageBitmap(bitmap);
    }

}


