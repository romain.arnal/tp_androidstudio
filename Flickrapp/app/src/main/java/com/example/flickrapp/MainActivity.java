package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONObject;

// Q12 - Il faudrait supprimer la partie " jsonFlickrFeed(...) "
public class MainActivity extends AppCompatActivity {
    public Bundle savedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_main);
        Button b = findViewById(R.id.button);
        ImageView i = findViewById(R.id.imageView2);
        b.setOnClickListener(new GetImageOnClickListener(i));
        Button blist = findViewById(R.id.button2);
        blist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listActivityIntent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(listActivityIntent);
            }
        });

        Log.v("fin","je suis a la fin de main");

    }

}
