package com.example.flickrapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MyAdapter extends BaseAdapter {
    private Context context;

    private Vector<String> vector ;
    private int count;

    public MyAdapter(Context context){
        this.vector = new Vector<>();
        count = 0;
        this.context = context;
    }

    @Override
    public int getCount() {

        return count;
    }

    @Override
    public String getItem(int i) {
        return vector.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
/*
        List<TextView> textViews = new ArrayList<>();

        if(view==null){
            view = LayoutInflater.from(context).inflate(R.layout.textviewlayout,viewGroup,false);
        }
        TextView text = view.findViewById(R.id.textView2);
        text.setText(getItem(i));
*/
        RequestQueue queue = MySingleton.getInstance(viewGroup.getContext()).
                getRequestQueue();
        if(view==null){
            view = LayoutInflater.from(context).inflate(R.layout.bitmaplayout,viewGroup,false);
        }
        ImageView image1 = view.findViewById(R.id.imageView);
        Log.i("dowlooad",getItem(i));
        ImageRequest request = new ImageRequest(
                getItem(i), new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                Log.i("dowlooad","finish");
                image1.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP,
                null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Some Thing Goes Wrong", Toast.LENGTH_SHORT).show();
                error.printStackTrace();

            }
        });
        queue.add(request);
        return view;
    }

    public void dd(String url){
        count+=1;
        vector.add(url);
        Log.i("JFL", "Adding to adapter url : " + url);
        notifyDataSetChanged();
    }
}

