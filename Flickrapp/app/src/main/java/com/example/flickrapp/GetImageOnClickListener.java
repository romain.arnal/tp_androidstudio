package com.example.flickrapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

public class GetImageOnClickListener implements View.OnClickListener {
    public JSONObject json = new JSONObject();
    public ImageView i;
    public GetImageOnClickListener(ImageView i) {
        this.i = i;
    }


    @Override
    public void onClick(View v) {
        Log.v("s","start");
        //AsyncFlickrJSONData async = new AsyncFlickrJSONData("test",null,json);
        //async.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
        AsyncTask<String, Void, JSONObject> s = new AsyncFlickrJSONData(i).execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
        Log.v("s","finish");

    }
}
